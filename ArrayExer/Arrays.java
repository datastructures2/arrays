package Array;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Arrays {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int size;
        while(true){
            System.out.print("Please enter the size of array: ");
            String sizeStr = sc.next();
            if(!isInteger(sizeStr)){
                System.out.println("Your input is not valid, it must be an number.");

            }else{

                if(Integer.parseInt(sizeStr) == 0){
                    System.out.println("Your input number must be more than 0.");
                }else{
                    size = Integer.parseInt(sizeStr);
                    break;
                }

            }
        }

        int[] intArr = new int[size];
        Random rand = new Random();
        int upperbound = 100;
        for(int i = 0; i < intArr.length; i++){
            intArr[i] = rand.nextInt(upperbound);
        }

        int count = 0;
        ArrayList primeList = new ArrayList();
        for(int i = 0; i<intArr.length; i++){

            if(i == intArr.length - 1){
                System.out.println(intArr[i]);
            }else{
                System.out.print(intArr[i] + ",");
            }
            if(isPrime(intArr[i])){
                primeList.add(intArr[i]);
                count++;
            }

        }

        if(count == 0){
            System.out.println("There is no prime numbers in the array.");
        }else{
            System.out.print("The Prime number in the array: ");
            for(int i = 0; i < primeList.size(); i++){
                if(i == primeList.size() - 1){
                    System.out.println(primeList.get(i) + "");
                }else{
                    System.out.print(primeList.get(i) + ",");
                }
            }
        }
    }
    public static boolean isInteger(String str){
        Pattern pattern = Pattern.compile("^[0-9]*$");
        Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }

    public static boolean isPrime(int intNum){
        if(intNum <= 3){
            return intNum > 1;
        }
       for(int i = 2; i < intNum; i++){
           if(intNum % i == 0){
               return false;
           }
       }
       return true;
    }


}
