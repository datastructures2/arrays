package Arrays;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class twoDimensionalArrayExer {
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        int[][] data;
        int totalNum = 0;
       // double numAvg = 0;
        ArrayList intList = new ArrayList();


        try{
            System.out.print("Enter the row numbers of two dimensional array:");
            int row = sc.nextInt();
            if(row < 1){
                throw new IllegalArgumentException("row must be more than 1");
            }
            System.out.print("Enter the column numbers of two dimensional array: ");
            int columns = sc.nextInt();
            if(columns < 1){
                throw  new IllegalArgumentException("column must be more than 1.");
            }
            data = new int[row][columns];
            for(int i = 0; i < row; i++){
                for(int j = 0; j < columns; j++){
                    data[i][j] = -99 + new Random().nextInt(199);
                }
            }

            for(int i = 0; i < row; i++){

                for(int j = 0; j < columns; j++){
                    System.out.printf("%s%d",j == 0 ? "" : ", ",data[i][j]);
                    intList.add(data[i][j]);
                    totalNum++;
                }
                System.out.println();
            }
            int total = getTotal(data);
            System.out.println("The sum of all numbers in the array is: " + total);
            System.out.println("There are " + totalNum + " numbers in array.");
            //numAvg = total / totalNum * 1.0;
            System.out.println("**********************************");

            listRowSum(data);
            listColumnSum(data);
            double std = getStd(data, totalNum);
            System.out.println("Standard deviation of all numbers in array is " + std);

            int twoSum = 0;
            int primeCount = 0;
            for(int i = 0; i < intList.size(); i++){
                for(int j = i + 1; j < intList.size(); j++){
                    twoSum = (int)intList.get(i) + (int)intList.get(j);
                    //System.out.println(intList.get(i) + " and " + intList.get(j) + " = " + twoSum);
                    if(isPrime(twoSum)){
                        primeCount++;
                        System.out.println(intList.get(i) + " + " + intList.get(j) + " = " + twoSum);
                    }
                }
            }

            if(primeCount == 0){
                System.out.println("There are no sum of two numbers is prime number.");
            }

        }catch (Exception exc){
            System.out.println(exc.getMessage());
        }

    }

    private static void listRowSum(int[][] data) {
        int rowTotal;
        for(int i = 0; i < data.length; i++){
            rowTotal = 0;
            for(int j = 0; j < data[0].length; j++){
                rowTotal += data[i][j];
            }
            System.out.println("Sum of row " + (i+1) + " is " + rowTotal);
        }
    }

    private static void listColumnSum(int[][] data) {
        int colTotal;
        for(int i = 0; i < data[0].length; i++){
            colTotal = 0;
            for(int j = 0; j < data.length; j++){
                colTotal += data[j][i];
            }
            System.out.println("Sum of column " + (i + 1) + " is " + colTotal);
        }
    }

    private static double getStd(int[][] data, int totalNum) {
        double tempTotal = 0;
        double std = 0;
        int total = getTotal(data);
        double numAvg = total / totalNum * 1.0;
        for(int i = 0; i < data.length; i++){
            for(int j = 0; j < data[0].length; j++){
               tempTotal += Math.pow(data[i][j] - numAvg, 2);
            }
        }
        std = Math.sqrt(tempTotal/ totalNum -1);
        return std;
    }

    private static boolean isPrime(int num){
        if(num <= 3){
            return num > 1;
        }
        for(int i = 2; i < num; i++){
            if(num % i == 0){
                return false;
            }
        }
        return true;
    }

    private static int getTotal(int[][] arr){
        int total = 0;
        for(int i = 0; i < arr.length; i++){
            for(int j = 0; j < arr[0].length; j++){
                total += arr[i][j];
            }
        }
        return total;
    }
}
